k_pearlsedge = {
	1000.1.1 = { change_development_level = 8 }
	980.1.1 = {
		holder = 3	#Aron Pearlman
	}
}

d_pearlywine = {
	1000.1.1 = { change_development_level = 9 }
}

c_jewelpoint = {
	1000.1.1 = { change_development_level = 9 }
}

c_the_pearls = {
	1000.1.1 = { change_development_level = 6 }
}

k_tretun = {
	1000.1.1 = { change_development_level = 8 }
}

c_tretun = {
	1000.1.1 = { change_development_level = 9 }
}

d_roilsard = {
	1000.1.1 = { change_development_level = 11 }
}

c_roilsard = {
	1001.1.1 = { change_development_level = 12 }
	980.1.1 = {
		holder = 6	#Gregoire Roilsard
	}
}

c_saloren = {
	998.3.23 = {
		holder = 501	#Stefan sil Saloren
	}
}

c_vivinmar = {
	989.4.15 = {
		holder = 508
	}
	1001.1.12 = {
		holder = 4	#Petrus sil Vivin
	}
}


c_loopuis = {
	980.1.1 = {
		holder = 5	#Caylen sil na Loop
	}
}

k_carneter = {
	930.6.8 = {
		holder = 25 #Galien Dameris
	}
	980.12.1 = {
		holder = 24 #Marven Dameris
	}
	998.2.1 = {
		holder = 13 #Auci Dameris
	}
	1021.10.3 = {
		holder = 9 #Crege Dameris
	}
}

d_carneter = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.3 = {
		holder = 9 #Crege Dameris
	}
}

c_carneter = {
	1021.10.3 = {
		holder = 9 #Crege Dameris
	}
}

c_woodwell = {
	1001.1.1 = { change_development_level = 7 }
	1021.10.3 = {
		holder = 9 #Crege Dameris
	}
}

c_timberfort = {
	1001.1.1 = { change_development_level = 7 }
}

c_ancards_crossing = {
	1021.10.3 = {
		holder = 9 #Crege Dameris
	}
}
