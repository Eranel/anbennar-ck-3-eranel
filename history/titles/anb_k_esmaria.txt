d_silverforge = {
	952.1.7 = {
		holder = 68 #Dhaine Silverhammer
		government = feudal_government
	}
}

c_silverforge = {
	952.1.7 = {
		holder = 68 #Dhaine Silverhammer
		government = feudal_government
	}
}

c_calascandar = {
	1020.4.5 = {
		holder = 22 #Calasandur
		government = feudal_government
		liege = "k_arbaran"
	}
}

d_cann_esmar = {
	1005.7.3 = {
		holder = 69 #Laren Estallen
		government = feudal_government
	}
}

d_cann_esmar = {
	1005.7.3 = {
		holder = 69 #Laren Estallen
		government = feudal_government
	}
}

d_aranmas = {
	1005.7.3 = {
		holder = 69 #Laren Estallen
		government = feudal_government
	}
}

d_bennon = {
	1018.8.19 = {
		holder = 70 #Jaye Bennon
		government = feudal_government
	}
}

d_estallen = {
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
		government = feudal_government
	}
}

d_songbarges = {
	1019.2.1 = {
		holder = 71 #Lindelin sil Estallen
		government = feudal_government
	}
}

d_ryalanar = {
	1010.12.14 = {
		holder = 72 #Eren Ryalan
		government = feudal_government
	}
}

d_konwell = {
	1010.12.14 = {
		holder = 72 #Eren Ryalan
		government = feudal_government
	}
}

d_leslinpar = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
		government = feudal_government
	}
}

d_hearthswood = {
	1019.3.5 = {
		holder = 73 #Leslin sil Leslinpár
		government = feudal_government
	}
}

d_asheniande = {
	1005.10.31 = {
		holder = 74 #Arlen Havoran
		government = feudal_government
	}
}