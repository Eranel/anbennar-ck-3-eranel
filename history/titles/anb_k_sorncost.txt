k_sorncost = {
	1000.1.1 = { change_development_level = 8 }
	983.5.1 = {
		holder = 511	#Petran of Sorncost
	}
	1019.2.5 = {
		holder = 8	#Alcuin sil Sorncost
	}
}

d_sorncost = {
	1000.1.1 = { change_development_level = 8 }
}

c_sorncost = {
	1000.1.1 = { change_development_level = 12 }
}

c_fioncavin = {
	1000.1.1 = { change_development_level = 11 }
}

d_sormanni_hills = {
	1000.1.1 = { change_development_level = 7 }
}

d_venail = {
	1000.1.1 = { change_development_level = 5 }
}

c_venail = {
	1000.1.1 = { change_development_level = 8 }
}

c_coruan = {
	1000.1.1 = { change_development_level = 9 }
}