d_wexhills = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

d_ottocam = {
	1015.6.7 = {
		holder = 65 #Dyren Wex
	}
}

d_bisan = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

d_greater_bardswood = {
	1005.9.2 = {
		holder = 67 #Hargen Bisan
	}
}

d_sugamber = {
	970.3.4 = {
		holder = 66 #Rycroft Sugambic
	}
}

d_escandar = {
	1000.1.1 = { change_development_level = 6 }
	1017.1.1 = {
		holder = 551 #Magda asil Magda
	}
}

c_magdalaire = {
	1000.1.1 = { change_development_level = 9 }
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

c_arca_magda = {
	1015.3.21 = {
		holder = 551 #Magda sil Magda
	}
}

b_aelthil = {
	1015.3.21 = {
		holder = 552 #Celarion, Magda's elven husband
	}
}