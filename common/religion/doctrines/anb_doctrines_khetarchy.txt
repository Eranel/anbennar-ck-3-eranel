doctrine_khet = {
	group = "main_group"
	is_available_on_create = {
		religion_tag = khetism_religion
	}
	
	#############################
	# 		Khetism Tenets		#
	#############################
	
	doctrine_khetarchy = {
		icon = khetarchy     
		name = doctrine_khetarchy_name

		piety_cost = {
			value = 0
		}
		parameter = {
			desc = anb_khet_mandate
		}

		character_modifier = {
			floodplains_advantage = 5
		}
	}
}