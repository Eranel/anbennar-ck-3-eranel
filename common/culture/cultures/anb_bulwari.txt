﻿bulwari_group = {
	graphical_cultures = {
		arabic_group_coa_gfx
		mena_building_gfx
		mena_clothing_gfx
		mena_unit_gfx
		dde_abbasid_clothing_gfx
	}
	mercenary_names = {
	}

	zanite = {
		
		color = { 213 98 117 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = arab
		}
	}

	brasanni = {
		
		color = { 205 233 232 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = arab
		}
	}

	gelkar = {
		
		color = { 54 184 198 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			25 = arab
			75 = mediterranean_byzantine
		}
	}

	bahari = {
		
		color = { 126 213 89 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			50 = arab
			50 = mediterranean_byzantine
		}
	}

	surani = {
		
		color = { 245 207 137 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = arab
		}
	}
	
	sadnatu = {
		
		color = { 60 45 10 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = arab
		}
	}
	
	masnsih = {
		
		color = { 200 105 50 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = arab
		}
	}
	
	maqeti = {
		
		color = { 190 190 50 }
		
		cadet_dynasty_names = {
			{ "dynnp_d_" "dynn_Merovingian" }
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		dynasty_names = {
			{ "dynnp_d_" "dynn_Aubergine" }
		}

		male_names = {

			#From EU4
			Aban Abazu Absol Adad Akur Aloros Anshar Arad Arashk Arhab Arsham Arvand Ashan Ashur Asor Atbin Athan Awbal Baraz Barid Barseen Bavar Behlil Berk 
			Bullud Bupalos Cahit Dagan Darab Darian Dariush Dartaxes Demartos Demos Durul Duzan Duzar Duzi Ekrem Erbay Erol Erten Fazil Giz Hazan Hermeias Himmet 
			Huran Husaam Hyaclos Idad Idris Ikelos Irran Iskrates Jahangir Jareer Kalib Kaptan Kartal Kaveh Kikud Korydon Laris Larza Limer Lykidas Marod Marodeen 
			Mehran Mesten Nahroon Naram Numair Oktan Oreias Panthes Raafi Rabon Radin Rimush Saamir Sabum Saed Samiun Sargin Sepeh Serim Sharaf Suhlamu Temenos Theron 
			Tozan Tydeos Udish Ufuk Yazkur Zaid Zamman Zimud Zimudar Maqet Dasma
		}
		female_names = {
			Aamina Abella Amata Amina Anatu Aqeela Araha Arahuna Arwia Asiah Baila Benan Canfeda Damrina Derya Dilara Elif Elina Elmas Emmita Ettu Fatma Gula Haala Haamida 
			Hasibe Ia Iltani Ishtara Jannat Kamelya Kishar Kullaa Leja Lubna Mardina Mudam Musheera Nadide Nahrina Nardina Nesrin Ninsunu Nuriye Nusaiba Nuzha Omara Pari Qamara 
			Rahma Saba Sabahat Samra Semiha Seyma Sireen Ura Yamima Yamina Zakiti Zeyni
		}

		dynasty_of_location_prefix = "dynnp_szel"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			100 = arab
		}
	}
}