﻿###################################################
# GENERAL ONGOING EVENTS FOR SPELLCASTING
###################################################

namespace = anb_spellcasting_general_ongoing_events


#Relevant passage in ancient spellbook found
anb_spellcasting_general_ongoing_events.1 = {
	type = character_event
	title = anb_spellcasting_general_ongoing_events.1.t
	desc = anb_spellcasting_general_ongoing_events.1.d

	left_portrait = scope:owner

	theme = learning

	trigger = {
		scope:owner.learning > poor_skill_level
		scope:scheme = {
			NOT = { has_variable = had_risky_spellbook_event }
			NOT = { has_variable = risky_spellbook_taken }
		}
	}

	immediate = {
		scope:scheme = {
			set_variable = {
				name = had_risky_spellbook_event
				value = yes
				years = 3
			}
		}
	}

	#A lucky find
	option = {
		name = anb_spellcasting_general_ongoing_events.1.a
		scope:scheme = {
			add_scheme_modifier = {
				type = general_spellcasting_spellbook_advice
			}
		}
		scope:scheme = {
			set_variable = {
				name = risky_spellbook_taken
				value = yes
			}
		}
	}

	#Plumb the depths
	option = {
		name = anb_spellcasting_general_ongoing_events.1.b
		duel = {
			skill = learning
			value = average_skill_rating
			75 = {
				desc = anb_spellcasting_general_ongoing_events.1.b.success
				compare_modifier = {
					value = scope:duel_value
					multiplier = 5
				}
				send_interface_toast = {
					title = anb_spellcasting_general_ongoing_events.1.b_toast
					left_icon = scope:owner
					scope:scheme = {
						add_scheme_modifier = {
							type = general_spellcasting_spellbook_advice_deep
						}
					}
				}
			}
			25 = {	
				desc = anb_spellcasting_general_ongoing_events.1.b.failure
				compare_modifier = {
					value = scope:duel_value
					multiplier = -5
				}
				min = 10
			}
		}
		stress_impact = {
			base = minor_stress_impact_gain
		}
		scope:scheme = {
			set_variable = {
				name = risky_spellbook_taken
				value = yes
			}
		}
	}

	#Too risky
	option = {
		name = anb_spellcasting_general_ongoing_events.1.c
		stress_impact = {
			brave = minor_stress_impact_gain
		}
	}
}

#Stress of preparing spell leaves toll on health
anb_spellcasting_general_ongoing_events.2 = {
	type = character_event
	title = anb_spellcasting_general_ongoing_events.2.t
	desc = anb_spellcasting_general_ongoing_events.2.d

	left_portrait = scope:owner

	theme = medicine

	trigger = {
		is_healthy = yes
		NOT = { has_character_modifier = general_spellcasting_tired }
	}

	#Push past it
	option = {
		name = anb_spellcasting_general_ongoing_events.2.a
		add_character_modifier = {
			modifier = general_spellcasting_tired
			years = 3
		}
		scope:scheme = {
			add_scheme_modifier = {
				type = general_spellcasting_push_past_health
				years = 3
			}
		}
		stress_impact = {
			lazy = minor_stress_impact_gain
		}
	}

	#Take it easy for a bit
	option = {
		name = anb_spellcasting_general_ongoing_events.2.b
		stress_impact = {
			base = minor_stress_impact_loss
			diligent = medium_stress_impact_gain
		}
		scope:scheme = {
			add_scheme_progress = -1
		}
	}
}

#Target starts to have doubts about what you are doing
anb_spellcasting_general_ongoing_events.3 = {
	type = character_event
	title = anb_spellcasting_general_ongoing_events.3.t
	desc = anb_spellcasting_general_ongoing_events.3.d

	theme = unfriendly

	trigger = {
		NOT = { scope:owner = scope:target }
		NOT = { scope:target = { has_trait = trusting } }
		NOT = { scope:scheme = { has_scheme_modifier = general_spellcasting_target_has_doubts } }
	}

	weight_multiplier = {
		base = 1
		#More likely to happen if target is paranoid
		modifier = {
			add = 0.5
			scope:target = { has_trait = paranoid }
		}
		#Or if you do not have any magical affinity
		modifier = {
			add = 1
			scope:owner = { has_magical_affinity = no }
		}
	}

	lower_left_portrait = scope:owner
	right_portrait = {
		trigger = { NOT = { scope:owner = scope:target } }
		character = scope:target
		animation = paranoia
	}

	#Give them assurances
	option = {
		scope:scheme = {
			add_scheme_modifier = {
				type = general_spellcasting_target_has_doubts
				years = 3
			}
		}
	}

	#Maybe we shouldn't do this afterall
	option = {
		scope:scheme = {
			end_scheme = yes
		}
	}
}

#Ingest Damestear powder
anb_spellcasting_general_ongoing_events.4 = {
	type = character_event
	title = anb_spellcasting_general_ongoing_events.4.t
	desc = anb_spellcasting_general_ongoing_events.4.d

	left_portrait = scope:owner

	theme = medicine

	trigger = {
		NOT = { scope:scheme = { has_scheme_modifier = general_spellcasting_damestear_powder } }
		NOT = { scope:target = { has_character_modifier = general_spellcasting_damestear_powder_other } }
	}
	#One for me please
	option = {
		name = anb_spellcasting_general_ongoing_events.4.a
		add_gold = -50
		scope:scheme = {
			add_scheme_modifier = {
				type = general_spellcasting_damestear_powder
				years = 1
			}
		}
		stress_impact = {
			base = minor_stress_impact_gain
		}
	}

	#Offer it to the target instead?
	option = {
		trigger = { NOT = { scope:owner = scope:target } }
		name = anb_spellcasting_general_ongoing_events.4.b
		add_gold = -50
		scope:scheme = { 
			add_scheme_modifier = {
				type = general_spellcasting_damestear_powder_other
				years = 1
			}
		}
	}

	#Too risky
	option = {
		name = anb_spellcasting_general_ongoing_events.4.c
	
	}
}